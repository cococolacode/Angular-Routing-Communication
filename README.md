## Getting started
  To pass data to the routed component,concatenate */:data* to the url location as follows,
 ``` javascript 
  const routes:Routes=[
  {path:'details/:id',component:DetailsComponent}
  ]
  ```
  
 At the time of navigation, pass the data as follows,
  ``` html
  <a class="btn btn-white-view" [routerLink]="[ '/details',"detail1"]">details</a>
  ```
    
  At details.component.ts
  ``` javascript
  import { ActivatedRoute } from '@angular/router'
  
  exports class DetailsComponent{
  constructor(private route:ActivatedRoute){};
   
    ngOnInit() {
           this.route.params.subscribe(details => {
           console.log(details);
              });
         }
     }   
   ```
   *But, what? if you want to pass object. It can be done by following ways*
   
   While routing,
   ``` javascript
   const routes:Routes=[
  {path:'details',component:DetailsComponent}
  ]
  ```
  
  At details.component.html,
 ``` html
     <a class="btn btn-white-view" [routerLink]="[ '/details'],
     {queryParams:{data:JSON.stringify(details)}">details</a>
  ```
  
  At details.component.ts
  ``` javascript
  import { ActivatedRoute } from '@angular/router'
  
  exports class DetailsComponent{
  constructor(private route:ActivatedRoute){};
   
    ngOnInit() {
           this.route.queryParams.subscribe(details => {
           console.log(details);
             });
         }
     }   
   ```
   
   
  
  
